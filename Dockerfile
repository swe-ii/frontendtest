FROM node:latest as build-stage
WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./ /app/
ARG configuration=production
RUN npm run build -- --output-path=./dist/out --configuration $configuration
RUN npm audit fix

FROM angular-nginx-docker
COPY --from=build-stage /app/dist/out/ /usr/share/nginx/html
#COPY /nginx-custom.conf /etc/nginx/conf.d/default.conf
EXPOSE 8080

