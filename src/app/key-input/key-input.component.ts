import {Component, OnInit} from '@angular/core';
import {DataService} from '../services/dataservice/data.service';
import {VocableContainer} from '../types/VocableContainer.type';
import {RequestService} from '../services/requestservice/request.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-key-input',
  templateUrl: './key-input.component.html',
  styleUrls: ['./key-input.component.scss']
})
export class KeyInputComponent implements OnInit {

  userInput: string;
  vocableContainer: VocableContainer;
  infoMessageKeyDoesNotExist: string;

  constructor(private dataService: DataService, private requestService: RequestService, private router: Router) {
    this.vocableContainer = new VocableContainer('0', '', '', '', '');
  }

  ngOnInit() {
  }

  public submitToTraining(): void {
    this.requestService.getContainerByShareableKey(this.userInput).subscribe(containerResponse => {
      Object.assign(this.vocableContainer, containerResponse);

      if (this.doesContainerExists(this.vocableContainer)) {
        this.dataService.changeVocableContainer(this.vocableContainer);
        this.router.navigateByUrl('/training');
      } else {
        this.infoMessageKeyDoesNotExist = 'Schlüssel existiert nicht.';
      }
    });
  }

  private doesContainerExists(vocableContainer: VocableContainer): boolean {
    if (vocableContainer.getVocableContainerId() !== '0') {
      return true;
    }

    return false;
  }
}
