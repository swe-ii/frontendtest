import {Component, OnInit} from '@angular/core';
import {Vocable} from '../types/Vocable.type';
import {DataService} from '../services/dataservice/data.service';
import {VocableContainer} from '../types/VocableContainer.type';
import {RequestService} from '../services/requestservice/request.service';

@Component({
  selector: 'app-container-vocablelist',
  templateUrl: './container-vocablelist.component.html',
  styleUrls: ['./container-vocablelist.component.scss']
})
export class ContainerVocablelistComponent implements OnInit {

  vocables: Array<Vocable>;
  vocableContainer: VocableContainer;
  loadingSpinner: boolean;

  constructor(private requestService: RequestService, private dataService: DataService) {
    this.vocables = new Array<Vocable>();
  }

  ngOnInit() {
    this.dataService.currentVocables.subscribe(vocables => {
      this.vocables = vocables;
    });

    this.dataService.currentVocableContainer.subscribe(vocableContainer => {
      this.vocableContainer = vocableContainer;
      this.updateVocables();
    });
  }

  public updateVocables(): void {
    this.requestService.getAllVocablesByVocableContainerId(this.vocableContainer.getVocableContainerId()).subscribe(result => {
      this.dataService.clearVocables();
      this.loadVocablesFromResult(result);
      this.dataService.changeVocables(this.vocables);
    });
  }

  public deleteVocable(vocable: Vocable): void {
    this.requestService.deleteVocable(vocable, this.vocableContainer).subscribe(() => {
      this.updateVocables();
    });
  }

  private loadVocablesFromResult(vocablesAsArray: Vocable[]): void {
    vocablesAsArray.forEach(vocable => {
      this.vocables.push(new Vocable(
        vocable.vocableId,
        vocable.vocableBaseLanguage,
        vocable.vocableGoalLanguage,
        vocable.wordClass,
        vocable.incorrectInputAmount,
        vocable.totalInputAmount
      ));
    });
  }
}
