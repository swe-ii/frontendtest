import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ContainerVocablelistComponent} from './container-vocablelist.component';

describe('ContainerVocablelistComponent', () => {
  let component: ContainerVocablelistComponent;
  let fixture: ComponentFixture<ContainerVocablelistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContainerVocablelistComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerVocablelistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
