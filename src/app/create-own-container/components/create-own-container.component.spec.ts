import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CreateOwnContainerComponent} from './create-own-container.component';

describe('CreateOwnContainerComponent', () => {
  let component: CreateOwnContainerComponent;
  let fixture: ComponentFixture<CreateOwnContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateOwnContainerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOwnContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
