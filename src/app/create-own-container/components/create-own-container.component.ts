import {Component, OnInit} from '@angular/core';
import {VocableContainer} from 'src/app/types/VocableContainer.type';
import {DataService} from 'src/app/services/dataservice/data.service';
import {Vocable} from 'src/app/types/Vocable.type';
import {RequestService} from 'src/app/services/requestservice/request.service';


@Component({
  selector: 'app-create-own-container',
  templateUrl: './create-own-container.component.html',
  styleUrls: ['./create-own-container.component.scss']
})
export class CreateOwnContainerComponent implements OnInit {

  vocables: Array<Vocable>;
  vocableContainer: VocableContainer;

  userInput: string;

  constructor(private dataService: DataService, private requestService: RequestService) {
    this.vocableContainer = new VocableContainer('0', '', '', '', '');
    this.dataService.currentVocables.subscribe(vocables => this.vocables = vocables);
    this.dataService.currentVocableContainer.subscribe(vocableContainer => this.vocableContainer = vocableContainer);
  }

  ngOnInit() {
  }

  loadContainer(): void {
    this.requestService.getContainerByShareableKey(this.userInput).subscribe(containerResponse => {
      Object.assign(this.vocableContainer, containerResponse);

      if (this.doesContainerExists) {
        this.dataService.changeVocableContainer(this.vocableContainer);
      }
    });
  }

  doesContainerExists(vocableContainer: VocableContainer): boolean {
    if (vocableContainer.getVocableContainerId() !== '0') {
      return true;
    }

    return false;
  }
}
