import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {ClarityModule} from '@clr/angular';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {WelcomeComponent} from './welcome/components/welcome.component';
import {TrainingComponent} from './training/components/training.component';
import {ImprintComponent} from './imprint/components/imprint.component';
import {CreateOwnContainerComponent} from './create-own-container/components/create-own-container.component';
import {WhoWeAreComponent} from './who-we-are/who-we-are.component';
import {CookieLawModule} from 'angular2-cookie-law';
import {CookieBannerComponent} from './cookie-banner/cookie-banner.component';
import {HeaderComponent} from './header/header.component';
import {ContainerDetailsComponent} from './container-details/container-details.component';
import {ContainerVocablelistComponent} from './container-vocablelist/container-vocablelist.component';
import {ContainerAddVocablesComponent} from './container-add-vocables/container-add-vocables.component';
import {KeyInputComponent} from './key-input/key-input.component';
import {RecaptchaModule} from 'ng-recaptcha';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    TrainingComponent,
    ImprintComponent,
    CreateOwnContainerComponent,
    WhoWeAreComponent,
    CookieBannerComponent,
    HeaderComponent,
    ContainerDetailsComponent,
    ContainerVocablelistComponent,
    ContainerAddVocablesComponent,
    KeyInputComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ClarityModule,
    BrowserAnimationsModule,
    CookieLawModule,
    RecaptchaModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
