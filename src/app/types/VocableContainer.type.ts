export class VocableContainer {
  private baseLanguage: string;
  private goalLanguage: string;
  private shareableKey: string;
  private title: string;
  private vocableContainerId: string;

  constructor($vocableContainerId: string, $baseLanguage: string, $goalLanguage: string, $shareableKey: string, $title: string) {
    this.vocableContainerId = $vocableContainerId;
    this.baseLanguage = $baseLanguage;
    this.goalLanguage = $goalLanguage;
    this.shareableKey = $shareableKey;
    this.title = $title;
  }

  public setVocableContainerId(value: string) {
    this.vocableContainerId = value;
  }

  public getBaseToGoalLanguageString(): string {
    return this.baseLanguage + '-' + this.goalLanguage;
  }

  public getBaseLanguage(): string {
    return this.baseLanguage;
  }

  public setBaseLanguage(baseLanguage: string): void {
    this.baseLanguage = baseLanguage;
  }

  public getGoalLanguage(): string {
    return this.goalLanguage;
  }

  public setGoalLanguage(goalLanguage: string): void {
    this.goalLanguage = goalLanguage;
  }

  public getShareableKey(): string {
    return this.shareableKey;
  }

  public setShareableKey(shareableKey: string): void {
    this.shareableKey = shareableKey;
  }

  public getTitle(): string {
    return this.title;
  }

  public setTitle(title: string): void {
    this.title = title;
  }

  public getVocableContainerId(): string {
    return this.vocableContainerId;
  }
}
