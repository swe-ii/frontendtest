export class Vocable {
  public vocableBaseLanguage: string;
  public vocableGoalLanguage: string;
  public incorrectInputAmount: number;
  public totalInputAmount: number;
  public vocableId: number;
  public wordClass: string;

  constructor($vocableId: number, $vocableBaseLanguage: string, $vocableGoalLanguage: string, $wordClass: string,
              incorrectInputAmount: number, totalInputAmount: number) {
    this.vocableBaseLanguage = $vocableBaseLanguage;
    this.vocableGoalLanguage = $vocableGoalLanguage;
    this.wordClass = $wordClass;
    this.vocableId = $vocableId;
    this.incorrectInputAmount = incorrectInputAmount;
    this.totalInputAmount = totalInputAmount;
  }

  public getVocableBaseLanguage(): string {
    return this.vocableBaseLanguage;
  }

  public getErrorQuote(): number {
    return this.incorrectInputAmount / this.totalInputAmount;
  }

  public getErrorQuoteFormatted(): string {
    if (Number.isNaN(this.getErrorQuote())) {
      return '-';
    }

    return (this.getErrorQuote() * 100).toFixed(2).toString();
  }

  public setVocableBaseLanguage(vocableBaseLanguage: string): void {
    this.vocableBaseLanguage = vocableBaseLanguage;
  }

  public getVocableGoalLanguage(): string {
    return this.vocableGoalLanguage;
  }

  public setVocableGoalLanguage(vocableGoalLanguage: string): void {
    this.vocableGoalLanguage = vocableGoalLanguage;
  }

  public getVocableId(): number {
    return this.vocableId;
  }

  public getWordClass(): string {
    return this.wordClass;
  }

  public setWordClass(wordClass: string): void {
    this.wordClass = wordClass;
  }
}
