import {Component} from '@angular/core';

@Component({
  selector: 'app-cookie-banner',
  template: `
  <cookie-law></cookie-law>
  `,
  styleUrls: ['./cookie-banner.component.scss']
})
export class CookieBannerComponent {
}
