import {Component, Input, OnInit} from '@angular/core';
import {VocableContainer} from '../types/VocableContainer.type';
import {Vocable} from '../types/Vocable.type';
import {DataService} from '../services/dataservice/data.service';
import {ContainerVocablelistComponent} from '../container-vocablelist/container-vocablelist.component';
import {RequestService} from '../services/requestservice/request.service';

@Component({
  selector: 'app-container-add-vocables',
  templateUrl: './container-add-vocables.component.html',
  styleUrls: ['./container-add-vocables.component.scss']
})
export class ContainerAddVocablesComponent implements OnInit {

  vocableBaseLanguage: string;
  vocableGoalLanguage: string;
  wordClass: string;
  infoMessageAddVocable: string;
  @Input() vocabellistSiblingComponent: ContainerVocablelistComponent;
  vocables: Array<Vocable>;
  vocableContainer: VocableContainer;

  constructor(private requestService: RequestService, private dataService: DataService) {
  }

  ngOnInit() {
    this.dataService.currentVocables.subscribe(vocables => this.vocables = vocables);
    this.dataService.currentVocableContainer.subscribe(vocableContainer => this.vocableContainer = vocableContainer);
  }

  public postVocableToBackend(): void {
    if (!this.checkIfFieldsAreEmpty()) {
      if (this.vocableContainer) {
        const vocable = new Vocable(0, this.vocableBaseLanguage, this.vocableGoalLanguage, this.wordClass, 0, 0);

        this.requestService.postVocable(vocable, this.vocableContainer.getVocableContainerId()).subscribe(data => {
          this.dataService.changeVocables(new Array<Vocable>());
          this.vocabellistSiblingComponent.updateVocables();
          this.clearUserInput();
          this.infoMessageAddVocable = 'Vokabel wurde hinzugefügt.';
        }, () => {
          this.infoMessageAddVocable = 'Vokabel konnte nicht hinzugefügt werden. Bitte erneut versuchen!';
        });
      }
    } else {
      this.infoMessageAddVocable = 'Vokabel konnte nicht hinzugefügt werden. Bitte eine Vokabel und dessen Übersetzung eintragen!';
    }
  }

  public setWarningIconVocableBaseLanguage(): boolean {
    if (this.checkIfVocableBaseLanguageIsEmpty()) {
      return false;
    }
    return true;
  }

  public setWarningIconVocableGoalLanguage(): boolean {
    if (this.checkIfVocableGoalLanguageIsEmpty()) {
      return false;
    }
    return true;
  }

  private checkIfFieldsAreEmpty(): boolean {
    return this.checkIfVocableBaseLanguageIsEmpty() || this.checkIfVocableGoalLanguageIsEmpty();
  }

  private checkIfVocableBaseLanguageIsEmpty(): boolean {
    return this.vocableBaseLanguage === undefined || this.vocableBaseLanguage == null || this.vocableBaseLanguage.trim().length === 0;
  }

  private checkIfVocableGoalLanguageIsEmpty(): boolean {
    return this.vocableGoalLanguage === undefined || this.vocableGoalLanguage == null || this.vocableGoalLanguage.trim().length === 0;
  }

  private clearUserInput(): void {
    this.vocableBaseLanguage = '';
    this.vocableGoalLanguage = '';
    this.wordClass = '';
  }
}
