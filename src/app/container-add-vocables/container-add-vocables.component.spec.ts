import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ContainerAddVocablesComponent} from './container-add-vocables.component';

describe('ContainerAddVocablesComponent', () => {
  let component: ContainerAddVocablesComponent;
  let fixture: ComponentFixture<ContainerAddVocablesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContainerAddVocablesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerAddVocablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
