import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/map';
import {Vocable} from '../../types/Vocable.type';
import {VocableContainer} from 'src/app/types/VocableContainer.type';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  BASE_URL = 'https://api.meinvokabeltrainer.com/';

  constructor(private http: HttpClient) {
  }

  public getAllVocablesByVocableContainerId(vocableContainerId: string): Observable<Vocable[]> {
    const REQUEST_URL: string = this.BASE_URL + 'vocable/' + vocableContainerId;

    return this.http.get<Vocable[]>(REQUEST_URL);
  }

  public getContainerById(vocableContainerId: number): Observable<VocableContainer> {
    const REQUEST_URL: string = this.BASE_URL + 'vocableContainers/' + vocableContainerId;

    return this.http.get<VocableContainer>(REQUEST_URL);
  }

  public getContainerByShareableKey(vocableContainerKey: string): Observable<VocableContainer> {
    const REQUEST_URL: string = this.BASE_URL + 'vocableContainers/byKey/' + vocableContainerKey;

    return this.http.get<VocableContainer>(REQUEST_URL);
  }

  public postUserInput(vocableId: number, userInput: string): any {
    const REQUEST_URL: string = this.BASE_URL + 'vocableComparing/' + vocableId + '/' + userInput;
    const BODY = null;

    return this.http.post(REQUEST_URL, BODY);
  }

  public postVocable(vocableToPost: Vocable, vocableContainerId: string): Observable<Vocable> {
    const REQUEST_URL: string = this.BASE_URL + 'vocable/' + vocableContainerId + '/';
    const HEADER = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('accept', 'application/json');

    return this.http.post<Vocable>(REQUEST_URL, vocableToPost, {headers: HEADER});
  }

  public postVocableContainer(vocableContainerToPost: VocableContainer, token: string): Observable<VocableContainer> {
    const REQUEST_URL: string = this.BASE_URL + 'vocableContainers/';
    const HEADER = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('accept', 'application/json')
      .set('recaptcha', token);

    return this.http.post<VocableContainer>(REQUEST_URL, vocableContainerToPost, {headers: HEADER});
  }

  public deleteVocable(vocable: Vocable, vocableContainer: VocableContainer): any {
    const REQUEST_URL: string = this.BASE_URL + 'vocable/' + vocableContainer.getVocableContainerId() + '/' + vocable.getVocableId();

    return this.http.delete(REQUEST_URL, {responseType: 'text'});
  }

  public deleteVocableContainer(vocableContainer: VocableContainer): any {
    const REQUEST_URL: string = this.BASE_URL + 'vocableContainers/' + vocableContainer.getVocableContainerId();

    return this.http.delete(REQUEST_URL, {responseType: 'text'});
  }
}
