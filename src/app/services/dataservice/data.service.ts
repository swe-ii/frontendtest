import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Vocable} from '../../types/Vocable.type';
import {VocableContainer} from '../../types/VocableContainer.type';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private vocablesSource = new BehaviorSubject(new Array<Vocable>());
  currentVocables = this.vocablesSource.asObservable();

  private vocableContainerSource = new BehaviorSubject(new VocableContainer('0', '', '', '', ''));
  currentVocableContainer = this.vocableContainerSource.asObservable();

  constructor() {
  }

  public changeVocables(vocables: Array<Vocable>): void {
    this.vocablesSource.next(vocables);
  }

  public changeVocableContainer(vocableContainer: VocableContainer): void {
    this.vocableContainerSource.next(vocableContainer);
  }

  public clearVocables(): void {
    this.changeVocables(new Array<Vocable>());
  }
}
