import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WelcomeComponent} from './welcome/components/welcome.component';
import {TrainingComponent} from './training/components/training.component';
import {ImprintComponent} from './imprint/components/imprint.component';
import {WhoWeAreComponent} from './who-we-are/who-we-are.component';
import {CreateOwnContainerComponent} from './create-own-container/components/create-own-container.component';
import {KeyInputComponent} from './key-input/key-input.component';

const routes: Routes = [
  {path: '', redirectTo: '/welcome', pathMatch: 'full'},
  {path: 'training', component: TrainingComponent},
  {path: 'welcome', component: WelcomeComponent},
  {path: 'key', component: KeyInputComponent},
  {path: 'imprint', component: ImprintComponent},
  {path: 'whoWeAre', component: WhoWeAreComponent},
  {path: 'createOwnContainer', component: CreateOwnContainerComponent},
  {path: '**', redirectTo: '/welcome', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
