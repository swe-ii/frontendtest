import {Component, OnInit} from '@angular/core';
import {RequestService} from '../../services/requestservice/request.service';
import {Vocable} from '../../types/Vocable.type';
import {VocableContainer} from '../../types/VocableContainer.type';
import {DataService} from 'src/app/services/dataservice/data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.scss']
})
export class TrainingComponent implements OnInit {
  vocableContainer: VocableContainer;
  vocables: Vocable[];
  vocableToTranslate: Vocable;
  userInput: string;
  resultDescription: string;
  lastVocableGoalLanguage: string;
  loadingSpinner: boolean;
  lastIndexesBuffer: Array<number>;
  maxBufferSize: number;
  parsedResponse: any;
  timeToResetAnimation = 20;

  constructor(private trainingService: RequestService, private dataService: DataService, private router: Router) {
    this.initializeVariables();
    this.lastIndexesBuffer = new Array<number>();
  }

  ngOnInit() {
    this.setSpinnerToLoading();
    this.vocables = new Array<Vocable>();
    this.parsedResponse = '';

    this.dataService.currentVocableContainer.subscribe(container => {
      this.vocableContainer = container;

      this.trainingService.getAllVocablesByVocableContainerId(this.vocableContainer.getVocableContainerId())
        .subscribe(vocables => {
          this.vocables = new Array<Vocable>();
          this.loadVocablesFromResponse(vocables);

          if (this.vocables.length > 0) {
            this.setSpinnerToNotLoading();
            this.selectVocableAndWaitForInput();
            this.calculateBufferSizeValue();
          }
        });
    });
  }

  public submitToVocableManagement(): void {
    this.trainingService.getContainerByShareableKey(this.userInput).subscribe(containerResponse => {
      Object.assign(this.vocableContainer, containerResponse);

      if (this.doesContainerExists(this.vocableContainer)) {
        this.dataService.changeVocableContainer(this.vocableContainer);
        this.router.navigateByUrl('/createOwnContainer');
      }
    });
  }

  public compareVocable(): void {
    if (this.checkIfComparisonIsPossible()) {
      return;
    }
    this.setSpinnerToLoading();

    this.userInput = this.userInput.trim();

    const lastUserInput = this.userInput;
    this.lastVocableGoalLanguage = this.vocableToTranslate.getVocableGoalLanguage();

    this.trainingService.postUserInput(this.vocableToTranslate.getVocableId(), this.userInput)
      .subscribe((comparingData: Response) => {
        this.handleUserInputCheckingResponse(comparingData, lastUserInput);
      }, () => {
        this.handleBackendErrorResponse();
      });
  }

  public checkParsedResponse(): boolean {
    if (this.parsedResponse.correct) {
      return true;
    } else {
      return false;
    }
  }

  private initializeVariables(): void {
    this.vocableToTranslate = new Vocable(0, '', '', '', 0, 0);
    this.vocableContainer = new VocableContainer('0', '', '', '', '');
  }

  private loadVocablesFromResponse(response: Vocable[]): void {
    response.forEach(vocableResponse => {
      this.vocables.push(new Vocable(
        vocableResponse.vocableId,
        vocableResponse.vocableBaseLanguage,
        vocableResponse.vocableGoalLanguage,
        vocableResponse.wordClass,
        vocableResponse.incorrectInputAmount,
        vocableResponse.totalInputAmount
      ));
    });
  }

  private selectVocableAndWaitForInput(): void {
    this.vocableToTranslate = this.vocables[this.getRandomIndexForVocablesWithBufferChecking()];
  }

  private resetUserInput(): void {
    this.userInput = '';
  }

  private checkIfComparisonIsPossible(): boolean {
    return this.userInput === undefined || this.userInput == null || this.userInput.length === 0
      || this.vocableToTranslate === undefined || this.vocableToTranslate == null
      || this.vocables === undefined || this.vocables == null || this.vocables.length === 0;
  }

  private doesContainerExists(vocableContainer: VocableContainer): boolean {
    if (vocableContainer.getVocableContainerId() !== '0') {
      return true;
    }

    return false;
  }

  private handleBackendErrorResponse(): void {
    this.setSpinnerToNotLoading();
    this.resetUserInput();
    this.resultDescription = 'Bei der Verarbeitung deiner Eingabe ist ein Fehler aufgereten. Bitte erneut versuchen.';
  }

  private addAnimationCorrectAnswer(): void {
    const element = document.getElementById('checkAnswerFooter');
    element.classList.add('correctAnswerFooter');
  }

  private addAnimationFalseAnswer(): void {
    const element = document.getElementById('checkAnswerFooter');
    element.classList.add('falseAnswerFooter');
  }

  private animateCorrectAnswer(): void {
    const element = document.getElementById('checkAnswerFooter');
    element.classList.remove('falseAnswerFooter');
    element.classList.remove('correctAnswerFooter');
    setTimeout(this.addAnimationCorrectAnswer, this.timeToResetAnimation);
  }

  private animateFalseAnswer(): void {
    const element = document.getElementById('checkAnswerFooter');
    element.classList.remove('falseAnswerFooter');
    element.classList.remove('correctAnswerFooter');
    setTimeout(this.addAnimationFalseAnswer, this.timeToResetAnimation);
  }

  private handleUserInputCheckingResponse(comparingResponse: Response, lastUserInput: string): void {
    this.parsedResponse = JSON.parse(JSON.stringify(comparingResponse));

    if (this.parsedResponse.correct) {
      this.resultDescription = 'Deine Eingabe "' + lastUserInput + '" war korrekt.';
      this.animateCorrectAnswer();
    } else {
      this.resultDescription = 'Deine Eingabe "' + lastUserInput + '" war falsch. Richtig wäre: ';
      this.animateFalseAnswer();
    }
    this.setSpinnerToNotLoading();
    this.selectVocableAndWaitForInput();
    this.resetUserInput();
  }

  private setSpinnerToLoading(): void {
    this.loadingSpinner = true;
  }

  private setSpinnerToNotLoading(): void {
    this.loadingSpinner = false;
  }

  private getRandomIndexForVocablesWithBufferChecking(): number {
    let index: number;

    do {
      index = Math.floor(Math.random() * this.vocables.length);
    } while (this.isVocableIdInsideLastIndexesBuffer(index));

    this.addLastVocableIdToLastIndexesBuffer(index);
    return index;
  }

  private calculateBufferSizeValue(): void {
    if (this.vocables.length > 4) {
      this.maxBufferSize = 4;
    } else if (this.vocables.length === 1) {
      this.maxBufferSize = 0;
    } else {
      this.maxBufferSize = 2;
    }
  }

  private addLastVocableIdToLastIndexesBuffer(vocableIdToAddToBuffer: number): void {
    this.lastIndexesBuffer.push(vocableIdToAddToBuffer);

    if (this.lastIndexesBuffer.length >= this.maxBufferSize) {
      this.lastIndexesBuffer.shift();
    }
  }

  private isVocableIdInsideLastIndexesBuffer(vocableIdToCheck: number): boolean {
    if (this.maxBufferSize === 0) {
      return false;
    }

    if (this.lastIndexesBuffer.indexOf(vocableIdToCheck) !== -1) {
      return true;
    }

    return false;
  }
}
