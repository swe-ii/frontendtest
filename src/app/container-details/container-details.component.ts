import {Component, OnInit} from '@angular/core';
import {VocableContainer} from '../types/VocableContainer.type';
import {DataService} from '../services/dataservice/data.service';
import {Vocable} from '../types/Vocable.type';
import {RequestService} from '../services/requestservice/request.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-container-details',
  templateUrl: './container-details.component.html',
  styleUrls: ['./container-details.component.scss']
})
export class ContainerDetailsComponent implements OnInit {

  title: string;
  baseLanguage: string;
  goalLanguage: string;
  shareableKey: string;
  infoMessageAddContainer: string;

  vocables: Array<Vocable>;
  vocableContainer: VocableContainer;

  captchaResponse: string;

  constructor(private requestService: RequestService, private dataService: DataService, private router: Router) {
  }

  ngOnInit() {
    this.dataService.currentVocables.subscribe(vocables => this.vocables = vocables);

    this.dataService.currentVocableContainer.subscribe(vocableContainer => {
      this.vocableContainer = vocableContainer;
      this.updateContainerDetails(vocableContainer);
    });
  }

  public postContainer(): void {
    if (!this.checkIfFieldsAreEmpty()) {
      const container = new VocableContainer('0', this.baseLanguage, this.goalLanguage,
        this.shareableKey, this.title);

      this.requestService.postVocableContainer(container, this.captchaResponse).subscribe(responseContainer => {
        Object.assign(container, responseContainer);
        this.dataService.changeVocableContainer(container);

        this.infoMessageAddContainer = 'Vokabelliste wurde erstellt.';
      }, () => {
        this.infoMessageAddContainer = 'Vokabelliste konnte nicht erstellt werden. Bitte erneut versuchen!';
      });
    } else {
      this.infoMessageAddContainer = 'Vokabelliste konnte nicht erstellt werden. Bitte leere Felder ausfüllen!';
    }
  }

  public deleteVocableContainer(): void {
    this.requestService.deleteVocableContainer(this.vocableContainer).subscribe(() => {
      const emptyVocableContainer = new VocableContainer('0', '', '', '', '');
      this.updateContainerDetails(emptyVocableContainer);
      this.dataService.changeVocableContainer(emptyVocableContainer);
    });
  }

  public resolved(captchaResponse: string) {
    this.captchaResponse = captchaResponse;
  }

  public setWarningIconTitle(): boolean {
    if (this.checkIfTitleIsEmpty()) {
      return false;
    }
    return true;
  }

  public setWarningIconBaseLanguage(): boolean {
    if (this.checkIfBaseLanguageIsEmpty()) {
      return false;
    }
    return true;
  }

  public setWarningIconGoalLanguage(): boolean {
    if (this.checkIfGoalLanguageIsEmpty()) {
      return false;
    }
    return true;
  }

  public submitToTraining(): void {
    if (this.checkIfContainerHasVocables()) {
      this.dataService.changeVocableContainer(this.vocableContainer);
      this.router.navigateByUrl('/training');
    } else {
      alert('Die Vokabelliste muss mindestens eine Vokabel enthalten, um zum Training zu gelangen!');
    }
  }

  private checkIfFieldsAreEmpty(): boolean {
    return this.checkIfBaseLanguageIsEmpty() || this.checkIfGoalLanguageIsEmpty() || this.checkIfTitleIsEmpty();
  }

  private checkIfBaseLanguageIsEmpty(): boolean {
    return this.baseLanguage === undefined || this.baseLanguage == null || this.baseLanguage.trim().length === 0;
  }

  private checkIfGoalLanguageIsEmpty(): boolean {
    return this.goalLanguage === undefined || this.goalLanguage == null || this.goalLanguage.trim().length === 0;
  }

  private checkIfTitleIsEmpty(): boolean {
    return this.title === undefined || this.title == null || this.title.trim().length === 0;
  }

  private checkIfContainerHasVocables(): boolean {
    if (Array.isArray(this.vocables) && this.vocables.length) {
      return true;
    }
    return false;
  }

  private updateContainerDetails(containerWithUpdates: VocableContainer): void {
    this.title = containerWithUpdates.getTitle();
    this.baseLanguage = containerWithUpdates.getBaseLanguage();
    this.goalLanguage = containerWithUpdates.getGoalLanguage();
    this.shareableKey = containerWithUpdates.getShareableKey();
  }
}
